# Changelog

All notable changes to this project will be documented in this file.

## [0.3.3] - 2020-03-26
### Added
- `-fsanitize=address,undefined` compiler flags in the `Makefile` to detect some type of bugs during compilation and linking.

## [0.3.2] - 2020-03-26
### Added
- `.clang-tidy` adjustment file for `clang-tidy` tool.
- Launching of `clang-tidy` static-analizer in `.gitlab-ci.yml`.

### Fixed
- Fix errors and bugs after applying `clang-tidy` static-analysis tool.
- Fix leak memory in overloaded `operator=`.

## [0.3.1] - 2020-03-26
### Added
- Launching of `cppcheck` static-analizer in `.gitlab-ci.yml` to detect bugs and errors.

## [0.3.0] - 2020-03-25
### Added
- `-Wall -Wextra -pedantic -Werror` compiler flags in `Makefile` to treat warning as errors.

### Changed
- `g++` compiler to `clang++` in the `Makefile`.

### Deleted
- `test` stage in .gitlab-ci.yml file as redundant. All test are build and run at the `build` stage.

## [0.2.0] - 2020-03-19
### Added
- [.gitlab-ci.yml](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/coder/pipeline/.gitlab-ci.yml) file to build and run unit tests on the GitLab CI/CD Pipeline.

## [0.1.0] - 2020-03-16
### Added
- Construcrtors and destructor of the [Coder](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/master/project/coder.cpp) class.
- Text decryption functionality by implementing `decode()` function in [coder.cpp](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/master/project/coder.cpp).
- Implementation of `size()`, `buf()` and `set()` functions in [coder.cpp](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/master/project/coder.cpp).
- Overloaded `operator=` and `deepcopy()` function in order to pass specific unit tests.
- [.clang-format](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/coder/solution/.clang-format) file to automatically format source code of [coder.h](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/master/project/coder.h) and [coder.cpp](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/master/project/coder.cpp) files.

### Deleted
- `#pragma pack` directive from [coder.h](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/master/project/coder.h) to change the alignment of class members in memory and to pass specific unit test in [coder_gTest.cpp](https://gitlab.com/procamp/19u4/trainees/serhii.unhurian/-/blob/coder/solution/test/coder_gTest.cpp).