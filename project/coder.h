/*
 * coder.h
 *
 *  Created for: GlobalLogic Basecamp
 *       Author: vitalii.lysenko
 *
 * Coder class header.
 *
 * You have to change this file to fix build errors, and make
 * the unit tests passed.
 *
 */

// NOLINTNEXTLINE Header guard is in accordance with [llvm-header-guard], no need to create it from the path name
#ifndef CODER_H
#define CODER_H

/*
 * This function was pre-compiled and is provided as a part of the
 * static library.
 *
 */
void encode(char *buf, int size);

/*
 * Coder class header.
 *
 */

// NOLINTNEXTLINE (hicpp-special-member-functions) Defining move constructor and move assignment operator would be redundant.
class Coder {
public:
  Coder();
  Coder(const Coder &source);
  ~Coder();

  void set(const char *buf, int size);
  char *buf() const;
  int size() const;

  void encode();
  void decode();
  Coder &operator=(const Coder &source);

private:
  char *_buf {nullptr};
  int _size {0};

  void deepCopy(const Coder &source);
};

#endif // CODER_H