/*
 * coder.cpp
 *
 *  Created for: GlobalLogic Basecamp
 *       Author: vitalii.lysenko
 *
 * Coder class source.
 *
 * You may have to change this file to fix build errors, and make
 * the unit tests pass.
 *
 * Usually source files are used to write an implementation of
 * global and member functions.
 *
 */

#include "coder.h"
#include <stdexcept>

/*
 * To make all unit tests pass you may try to puzzle out the ::encode()
 * algorithm and it should help you to write the decoding one. But there are
 * other ways to make them pass.
 *
 * Good luck!
 *
 */

Coder::Coder() = default;

Coder::Coder(const Coder &source) { deepCopy(source); }

Coder::~Coder() { delete[] _buf; }

void Coder::set(const char *buf, int size) {
  if (buf == nullptr || size <= 0) {
    throw std::invalid_argument("Invalid arguments");
  }
  _size = size;
  // NOLINTNEXTLINE (cppcoreguidelines-owning-memory)
  _buf = new char[_size];

  for (int i = 0; i < size; i++) {
    // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic) The expression is bounds-safe, there's no possibility to access memory outside of the range that was allocated for it.
    _buf[i] = buf[i];
  }
}

char *Coder::buf() const { return _buf; }

int Coder::size() const { return _size; }

Coder &Coder::operator=(const Coder &source) {
  if (this == &source) {
    return *this;
  }
  delete[] _buf; 
  
  deepCopy(source);

  return *this;
}

void Coder::encode() { ::encode(_buf, _size); }

void Coder::decode() {
  if (_buf == nullptr) {
    return;
  }
  
  int last = _size - 1;
  // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic) The expression is bounds-safe, there's no possibility to access memory outside of the range that was allocated for it.
  _buf[last] ^= 255;

  for (int i = last; i > 0; i--) {
    // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic) The expression is bounds-safe, there's no possibility to access memory outside of the range that was allocated for it.
    _buf[i - 1] ^= _buf[i];
  }
};

void Coder::deepCopy(const Coder &source) {
  _size = source.size();

  if (source.buf() != nullptr) {
    // NOLINTNEXTLINE (cppcoreguidelines-owning-memory) The expression is bounds-safe, there's no possibility to access memory outside of the range that was allocated for it.
    _buf = new char[_size];

    for (int i = 0; i < _size; i++) {
      // NOLINTNEXTLINE (cppcoreguidelines-pro-bounds-pointer-arithmetic) The expression is bounds-safe, there's no possibility to access memory outside of the range that was allocated for it.
      _buf[i] = source.buf()[i];
    }
  } else {
    _buf = nullptr;
  }
}
