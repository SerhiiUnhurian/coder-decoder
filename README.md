## Rrerequisites

- There is a declaration of Coder class in `coder.h` file.
- `encode()` was pre-compiled and was provided as a part of the static library. 

## Task Description
- Provide implementation for `Coder` class in `coder.cpp` file. 
- Implement `decode()` function, that decrypts ciphertext.
- Fix all build errors and make all unit tests provided in `coder_gTest.cpp` file pass.

The list of files which are allowed to be modified:

`project/coder.cpp` \
`project/coder.h` \
`Makefile`

## Project structure

```
coder/
├── Makefile
├── project
│   ├── coder.cpp
│   ├── coder.h
│   ├── libencode_cygwin_i686.a
│   ├── libencode_cygwin_x86_64.a
│   ├── libencode_linux_i386.a
│   ├── libencode_linux_x86_64.a
│   └── libencode_mac_x86_64.a
├── README.md
└── test
    └── coder_gTest.cpp

```